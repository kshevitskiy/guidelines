# Front-end coding guidelines

1. HTML
2. CSS
3. JS
4. 3rd party software
___




## 1. HTML
- 1.1 Naming `Kebab case`
- 1.2 Flags/keys
- 1.3 Methodology `BEM`

###### 1.1 Naming
Use only `Kebab case` syntax for classes and id's naming

Right
```html
<div class="class-name"></div>
```

Wrong
```html
<div class="className"></div>
```


###### 1.2 Flags/keys
Use flag `u-` for utilities, ie. `u-text-center`


###### 1.3 Methodology
Organize your HTML code using BEM methodology, ie:
```html
<form class="search-form">
    <div class="search-form__content">
        <input class="search-form__input">
        <button class="search-form__button">Search</button>
    </div>
</form>
```
Documentation: [https://en.bem.info/methodology/quick-start/](https://en.bem.info/methodology/quick-start/)

Notice: use two `--` hyphen-minus for modifier but not `_` underscore, ie: `--modifier`

___





## 2. CSS
- 2.1 Preprocessor `SASS`
- 2.2 File structure
- 2.3 Nesting
- 2.4 Mixins

###### 2.1 Preprocessor
___




## 3. JS
- 3.1 Naming

###### 3.1 Naming
___




## 4. 3rd party software

###### Developers and their plugins

- [Kushagra Gour](https://kushagragour.in/lab/)

###### Slider
- [Swiper](http://idangero.us/swiper/)

###### UI
- [Select2](https://select2.org/)
- [bootstrap-select](https://silviomoreto.github.io/bootstrap-select/)

###### Smooth scroll
- [smooth-scrollbar](https://github.com/idiotWu/smooth-scrollbar)

###### Events
- [in-view](https://github.com/camwiegert/in-view)
- [js-cookie](https://github.com/js-cookie/js-cookie)